#include "Depense.h"

Depense * createDep(float a, Type t, Date * date1){
    Depense *pt_dep=(Depense *)malloc(sizeof(Depense));
    pt_dep->date=createDate(date1->day, date1->month, date1->year);
    pt_dep->solde=a;
    pt_dep->type=t;
    return pt_dep;
}

Depense * copyDep(Depense * dep){
    return createDep(dep->solde,dep->type,dep->date);
}

void printDep(Depense *r1){
    printf("solde: %f type: %d date :%d %d %d\n",r1->solde,r1->type,r1->date->day,r1->date->month,r1->date->year);
}
