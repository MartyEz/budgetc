#include "Type.h"
#include "Date.h"
#include <stdio.h>
#ifndef REVENU_H_INCLUDED
#define REVENU_H_INCLUDED

typedef struct
{
    float solde;
    Type type;
    Date * date;

}Revenu;

Revenu * createRev(float a, Type t, Date * date);
Revenu * copyRev(Revenu * rev);
void printRev(Revenu *r1);

#endif
