#include "Type.h"
#include "Date.h"
#include <stdio.h>

#ifndef DEPENSE_H_INCLUDED
#define DEPENSE_H_INCLUDED
typedef struct
{
    float solde;
    Type type;
    Date * date;
}Depense;

Depense * createDep(float a, Type t, Date * date);
Depense * copyDep(Depense * dep);
void printDep(Depense *r1);
#endif

