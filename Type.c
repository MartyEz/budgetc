#include "Type.h"


int getType(char *str){
        if(strcmp(str,"Chauffage")==0 || strcmp(str,"Chauffage\0")==0)
            return 1;
        if(strcmp(str,"Maison")==0 || strcmp(str,"Maison\0")==0)
            return 0;
        if(strcmp(str,"Salaire")==0 || strcmp(str,"Salaire\0")==0)
            return 2;
        return 1;
}

char * getStrType(int a){
        char *rsl;
        switch(a){
        case 0:
            rsl="Maison";
            break;
        case 1:
            rsl="Chauffage";
            break;
        case 2:
            rsl="Salaire";
            break;
        }
        return rsl;
}
