#include "filemng.h"
#include <stdlib.h>
#include <stdio.h>


int main(int argc, char *argv[])
{
    /*Compte *cmp1=createCompte("ez");
    readAlarm("a.alarm",cmp1);
    printf("%d",cmp1->alarm->alarmes[0]);


    read("a",cmp1);*/





    int i;
    int j;
    char namecmp[25];
    Compte *cmp1;
    Revenu *r1;
    Depense *d1;
    Date *da1;
    char namealarm[25];
    int quit=0;
    int choix;
    int totalc;
    int cat;
    int montant;
    int day;
    int month;
    int year;

    printf("Bilenvenue dans Budget\n\n");
    if(argc==1){
        printf("Aucun nom de compte passé en argument. Veillez créer un compte en entrant un nom de compte\n\n");
        scanf("%s",namecmp);
        cmp1=createCompte(namecmp);
        createCompteFile(cmp1);
    }
    else
        cmp1=createCompte(argv[1]);



    strcpy(namealarm,cmp1->nom);
    strcat(namealarm,".alarm");



    read(cmp1->nom,cmp1);
    printf("\nCompte name : %s\n",cmp1->nom);
    printf("Solde : %f \n",total(cmp1));
    printf("Compte file : %s\n",cmp1->nom);
    printf("Alarme file : %s\n\n",namealarm);




    if(checkFileExist(namealarm)==1){


        readAlarm(namealarm,cmp1);

    }
    else if(checkFileExist(namealarm)==0){


        initAlarmeFile(namealarm);

        readAlarm(namealarm,cmp1);
    }

    while(quit==0){
        printf("Selectionnez un Choix : \n0-Calcul de totaux\n1-Nouveau Revenu\n2-Nouvelle Depense\n3-Alarmes\n4-Check alarmes\n5-Plot\n6-Quitter\n\n");
        scanf("%d",&choix);
        if(choix==6)
            quit=1;
        else if(choix==0){
            printf("Selectionnez un Choix :\n0-Total Global\n1-Total par Categorie\n\n");
            scanf("%d",&totalc);
            if(totalc==0){
                printf("Le solde total du compte est : %f\n\n",total(cmp1));
            }
            else if(totalc==1){
                printf("Selectionnez un Choix : \n0-Maison\n1-Chauffage\n2-Salaire\n\n");
                scanf("%d",&cat);
                printf("Le total de la categorie est %f\n\n",totalCat(cmp1,cat));
            }
        }
        else if(choix==1){
            printf("Selectionnez la categorie du revenu : \n0-Maison\n1-Chauffage\n2-Salaire\n");
            scanf("%d",&cat);
            printf("Selectionnez le montant du revenu :\n");
            scanf("%d",&montant);
            printf("Selectionnez le jour de l'opération :\n");
            scanf("%d",&day);
            printf("Selectionnez le mois de l'opération :\n");
            scanf("%d",&month);
            printf("Selectionnez l'année de l'opération :\n");
            scanf("%d",&year);
            da1=createDate(day,month,year);
            r1=createRev(montant,cat,da1);
            addRevenu(cmp1,r1);
            writeRev(cmp1->nom,r1);
            free(da1);
            free(r1->date);
            free(r1);
        }
        else if(choix==2){
            printf("Selectionnez la categorie de la Depense : \n0-Maison\n1-Chauffage\n2-Salaire\n");
            scanf("%d",&cat);
            printf("Selectionnez le montant de la depense :\n");
            scanf("%d",&montant);
            printf("Selectionnez le jour de l'opération :\n");
            scanf("%d",&day);
            printf("Selectionnez le mois de l'opération :\n");
            scanf("%d",&month);
            printf("Selectionnez l'année de l'opération :\n");
            scanf("%d",&year);
            da1=createDate(day,month,year);
            d1=createDep(montant,cat,da1);
            addDepense(cmp1,d1);
            writeDep(cmp1->nom,d1);
            free(da1);
            free(d1->date);
            free(d1);

        }
        else if (choix==3){
            printf("Selectionnez la categorie de l'alarme : \n0-Maison\n1-Chauffage\n2-Salaire\n");
            scanf("%d",&cat);
            printf("Selectionnez le seuil à ne pas dépasser :\n");
            scanf("%d",&montant);
            changeValAlarme(cmp1->alarm,cat,montant);
            updateAlarmFile(cmp1,namealarm);
        }
        else if (choix==4){
            char * s0=getStrType(0);
            char * s1=getStrType(1);
            char * s2=getStrType(2);
            printf("Alarme de %s : %d\nAlarme de %s : %d\nAlarme de %s : %d\n",s0,cmp1->alarm->alarmes[0],s1,cmp1->alarm->alarmes[1],s2,cmp1->alarm->alarmes[2]);
            if(totalCat(cmp1,0)<=cmp1->alarm->alarmes[0]){
                printf("SEUIL ATTEINT SUR %s\n",s0);
            }
            if(totalCat(cmp1,1)<=cmp1->alarm->alarmes[1]){
                printf("SEUIL ATTEINT SUR %s\n",s1);
            }
            if(totalCat(cmp1,2)<=cmp1->alarm->alarmes[2]){
                printf("SEUIL ATTEINT SUR %s\n",s2);
            }

        }
        else if(choix==5){
            printf("Graphique mensuel par année.\nSelectionnez un Choix : \n0-Graph total\n1-Graphique avec detail catégorie\n");
            scanf("%d",&choix);
            printf("Selectionnez l'année sur laquelle faire le graphique mensuel\n");
            scanf("%d",&year);
            if(choix==0){
                writePlot(".plot.tmp",cmp1,0,12,year);
                system("gnuplot -persist -e \"load \'plot.p\'\"");
            }
            if(choix==1){
                writePlotCat(".plot.tmp",cmp1,0,12,year);
                system("gnuplot -persist -e \"load \'plot.p\'\"");
            }
        }
   }




    for(i=0;i<cmp1->nbrev;i++){
        free((cmp1->revenus)[i]->date);
        free((cmp1->revenus)[i]);
    }

    for(j=0;j<cmp1->nbdep;j++){
        free((cmp1->depenses)[j]->date);
        free((cmp1->depenses)[j]);
    }

    free(cmp1->depenses);
    free(cmp1->revenus);
    free(cmp1->alarm->alarmes);
    free(cmp1->alarm);
    free(cmp1->nom);
    free(cmp1);



    return 0;
}
