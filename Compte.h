#include "Depense.h"
#include "Revenu.h"
#include "Alarme.h"
#include "Type.h"
#include <stdlib.h>
#include <string.h>

#ifndef COMPTE_H_INCLUDED
#define COMPTE_H_INCLUDED
typedef struct{
    char * nom;
    float solde;
    int nbrev;
    int nbdep;
    Alarme *alarm;
    Revenu **revenus;
    Depense **depenses;
}Compte;


Compte * createCompte(char * name);
float total(Compte * cmp);
int addRevenu(Compte *cmp,Revenu * rev);
int addDepense(Compte *cmp,Depense * dep);
float totalCat(Compte * cmp, Type type);
#endif


