#include <stdlib.h>
#ifndef DATE_H_INCLUDED
#define DATE_H_INCLUDED

typedef struct{
    int day;
    int month;
    int year;

}Date;

Date * createDate(int day, int month, int year);

#endif
