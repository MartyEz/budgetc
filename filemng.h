#include "Compte.h"
#include <stdlib.h>
#include <stdio.h>
#include "Revenu.h"
#include "Depense.h"
#include "Type.h"
#include "Date.h"
#include "Alarme.h"
#include <string.h>
#ifndef FILEMNG_H_INCLUDED
#define FILEMNG_H_INCLUDED



int read(char * filename, Compte *cmp);
int readAlarm(char * filename, Compte *cmp);
int writeRev(char * filename,Revenu *r1);
int writeDep(char * filename,Depense *r1);
int writePlot(char * filename, Compte *cmp,int motd,int motf, int year);
int checkFileExist(char * filename);
int initAlarmeFile(char * filename);
int updateAlarmFile(Compte * cmp, char *filename);
int writePlotScript(int a);
int writePlotCat(char * filename, Compte *cmp,int motd,int motf, int year);
int createCompteFile(Compte *cmp);

#endif
