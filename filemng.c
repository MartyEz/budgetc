#include "filemng.h"

#define INI_SIZE 5


int readAlarm(char * filename, Compte *cmp){

    FILE * dest;
    Alarme *al=createAlarme();
    char a1[20];
    char a2[20];
    char a3[20];
    int m1;
    int m2;
    int m3;
    char *line;
    int nbcline=40;

    dest=fopen(filename,"r");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }


    line=(char*)malloc(nbcline*sizeof(char));

    fgets(line,nbcline,dest);

    while(fgets(line,nbcline,dest)!=NULL){
        if(sscanf(line, "%[^,],%[^,],%[^,]\n",a1,a2,a3)==3){
            /*printf("%s %s %s\n",a1,a2,a3);*/
        }
    }

    fclose(dest);
    m1=atoi(a1);
    m2=atoi(a2);
    m3=atoi(a3);
    /*printf("%d %d %d\n",m1,m2,m3);*/

    changeValAlarme(al,0,m1);
    changeValAlarme(al,1,m2);
    changeValAlarme(al,2,m3);
    cmp->alarm=copyAlarme(al);
    free(al->alarmes);
    free(al);
    free(line);

    return 0;

}


int checkFileExist(char * filename){

    FILE *file=fopen(filename,"r");
    if (file != NULL) {
        fclose(file);
        return 1;
    }
    else{
       /* fclose(file);*/
        return 0;
    }
}

int initAlarmeFile(char * filename){
    FILE * dest;
    dest=fopen(filename,"w");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file in.list!\n");
        exit(1);
    }
   /* //printf("DEV FPRINTF\n");*/
    fprintf(dest,"%s\n","#Maison,Chauffage,Salaire");
    fprintf(dest,"%d,%d,%d\n",0,0,0);
    fclose(dest);
    return 0;
}

int updateAlarmFile(Compte * cmp, char *filename){
    FILE * dest;
    dest=fopen(filename,"w");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fprintf(dest,"%s\n","#Maison,Chauffage,Salaire");
    fprintf(dest,"%d,%d,%d\n",cmp->alarm->alarmes[0],cmp->alarm->alarmes[1],cmp->alarm->alarmes[2]);
    fclose(dest);
    return 0;
}

int read(char * filename, Compte *cmp){

    FILE * dest;
    char date[20];
    char type[20];
    char cat[20];
    char mont[20];
    char year[10];
    char month[10];
    char day[5];
    char *line;
    int nbcline=40;
    Date *da1;
    Revenu *r1;
    Depense *d1;

    dest=fopen(filename,"r");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }


    line=(char*)malloc(nbcline*sizeof(char));

    fgets(line,nbcline,dest);

    while(fgets(line,nbcline,dest)!=NULL){
        if(sscanf(line, "%[^,],%[^,],%[^,],%[^,]\n",date,type,cat,mont)==4){
            /*printf("%s %s %s\n",date,type,cat,mont);*/
            if(sscanf(date, "%[^/]/%[^/]/%[^/]",day,month,year)==3){
                /*printf("%s %s %s\n",day,month,year);*/
                da1=createDate(atoi(day),atoi(month),atoi(year));
                if(strcmp(type,"r")==0){

                    r1=createRev(atof(mont), getType(cat), da1);
                    printf("Create REVENU\n");
                    printRev(r1);
                    addRevenu(cmp,r1);
                    free(r1->date);
                    free(r1);
                }
                else if(strcmp(type,"d")==0){
                    d1=createDep(atof(mont), getType(cat), da1);
                    printf("Create DEPENSE\n");
                    printDep(d1);
                    addDepense(cmp,d1);
                    free(d1->date);
                    free(d1);
                }
                free(da1);
            }
        }
    }
    free(line);
    fclose(dest);
    return 0;
}

int writeRev(char * filename, Revenu *r1){
    FILE * dest;
    dest=fopen(filename,"a");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fprintf(dest,"\n");
    fprintf(dest,"%d/%d/%d,r,%s,%f",r1->date->day,r1->date->month,r1->date->year,getStrType(r1->type),r1->solde);
    fclose(dest);
    return 0;
}

int writeDep(char * filename, Depense *r1){
    FILE * dest;
    dest=fopen(filename,"a");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fprintf(dest,"\n");
    fprintf(dest,"%d/%d/%d,d,%s,%f",r1->date->day,r1->date->month,r1->date->year,getStrType(r1->type),r1->solde);
    fclose(dest);

    return 0;
}

int writePlotScript(int a){
    FILE * dest;
    dest=fopen("plot.p","w");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file in.list!\n");
        exit(1);
    }
    fprintf(dest,"%s\n","set autoscale");
    fprintf(dest,"%s\n","unset log");
    fprintf(dest,"%s\n","unset label");
    fprintf(dest,"%s\n","set xtic auto");
    fprintf(dest,"%s\n","set ytic auto ");
    fprintf(dest,"%s\n","set title \'Budget\'");
    fprintf(dest,"%s\n","set xlabel \'Month\'");
    fprintf(dest,"%s\n","set ylabel \'Value $\'");
    if(a==0)
        fprintf(dest,"%s\n","plot \'.plot.tmp\' u 1:2 title \'Total\' w l");
    else
        fprintf(dest,"%s\n","plot \'.plot.tmp\' u 1:2 title \'Total\' w l, \'\' u 1:3 title \'Maison\' w l, \'\' u 1:4 title \'Chauffage\' w l, \'\' u 1:5 title \'Salaire\' w l");
    fclose(dest);
    return 0;
}

int createCompteFile(Compte *cmp){
    FILE * dest;
    dest=fopen(cmp->nom,"w");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fclose(dest);

    return 0;
}

int writePlot(char * filename, Compte *cmp,int motd,int motf,int year){

    FILE * dest;
    int month;
    int nbrev=cmp->nbrev;
    int nbdep=cmp->nbdep;
    int i;
    int j;
    float sum=0;

    writePlotScript(0);

    dest=fopen(filename,"w");
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fprintf(dest,"%s\n","x y");


    for(month=motd;month<=motf;month++){
    /*printf("AV\n");*/
    for(i=0;i<nbrev;i++){
    if((cmp->revenus)[i]->date->year==year){
            if((cmp->revenus)[i]->date->month==month){
                sum+=(cmp->revenus)[i]->solde;

            }
        }
    }

    for(j=0;j<nbdep;j++){
    if((cmp->depenses)[j]->date->year==year){
        if((cmp->depenses)[j]->date->month==month){
            sum-=(cmp->depenses)[j]->solde;
        }
    }
    
    
    }
    fprintf(dest,"%d %f\n",month,sum);

}
fclose(dest);
    return 0;
}

int writePlotCat(char * filename, Compte *cmp,int motd,int motf, int year){

    FILE * dest;
    int month;

    int nbrev=cmp->nbrev;
    int nbdep=cmp->nbdep;
    int i;
    int j;
    float sum=0;
    float sum1=0;
    float sum2=0;
    float sum3=0;


    dest=fopen(filename,"w");
    writePlotScript(1);
    if(dest==NULL){
        fprintf(stderr, "Can't open input file\n");
        exit(1);
    }
    fprintf(dest,"%s\n","x y y1 y2 y3");


    for(month=motd;month<=motf;month++){
    for(i=0;i<nbrev;i++){
    if((cmp->revenus)[i]->date->year==year){
        if((cmp->revenus)[i]->date->month==month){
            sum+=(cmp->revenus)[i]->solde;

            if((cmp->revenus)[i]->type==0)
                sum1+=(cmp->revenus)[i]->solde;
            if((cmp->revenus)[i]->type==1)
                sum2+=(cmp->revenus)[i]->solde;
            if((cmp->revenus)[i]->type==2)
                sum3+=(cmp->revenus)[i]->solde;

        }
        }
    }



    for(j=0;j<nbdep;j++){
    if((cmp->depenses)[j]->date->year==year){
        if((cmp->depenses)[j]->date->month==month){
            sum-=(cmp->depenses)[j]->solde;

            if((cmp->depenses)[j]->type==0)
                sum1-=(cmp->depenses)[j]->solde;
            if((cmp->depenses)[j]->type==1)
                sum2-=(cmp->depenses)[j]->solde;
            if((cmp->depenses)[j]->type==2)
                sum3-=(cmp->depenses)[j]->solde;
        }
        }
    }

    fprintf(dest,"%d %f %f %f %f\n",month,sum,sum1,sum2,sum3);

    }
    fclose(dest);
    return 0;
}
