CC = gcc
OBJS = Alarme.o Compte.o Date.o Depense.o filemng.o main.o Revenu.o Type.o
CFLAGS = -c -Wall -pedantic
LDFLAGS = 
PGM = bud

all: $(PGM)

$(PGM): $(OBJS)
	$(CC) -o $(PGM) $(OBJS) $(LDFLAGS)

.c.o:
	$(CC) $(CFLAGS) $<
