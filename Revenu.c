#include "Revenu.h"


Revenu * createRev(float a, Type t, Date * date1){
    Revenu *pt_rev=(Revenu *)malloc(sizeof(Revenu));
    pt_rev->date=createDate(date1->day, date1->month, date1->year);
    pt_rev->solde=a;
    pt_rev->type=t;
    return pt_rev;
}

Revenu * copyRev(Revenu * rev){
    return createRev(rev->solde,rev->type,rev->date);
}


void printRev(Revenu *r1){
    printf("solde: %f type: %d date :%d %d %d\n",r1->solde,r1->type,r1->date->day,r1->date->month,r1->date->year);
}
