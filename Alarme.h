#include <stdlib.h>
#ifndef ALARME_H_INCLUDED
#define ALARME_H_INCLUDED
typedef struct{

    int *alarmes;

} Alarme;

Alarme * createAlarme();
void changeValAlarme(Alarme * a1,int a,int value);
Alarme * copyAlarme(Alarme * a1);


#endif
