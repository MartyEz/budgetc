#include "Compte.h"
#define MAX_TAB 5

int reallocntRev=1;
int reallocntDep=1;

Compte * createCompte(char *name){
    Compte *cmp1=(Compte *)malloc(sizeof(Compte));
    int sizename=sizeof(name)/sizeof(name[0]);
    cmp1->nom=(char *)malloc(sizename*sizeof(char));
    /*cmp1->alarm=(Alarme *)malloc(sizeof(Alarme));*/
    strcpy(cmp1->nom,name);
    cmp1->nbdep=0;

    cmp1->nbrev=0;
    reallocntRev=1;
    reallocntDep=1;
    cmp1->revenus=(Revenu**)malloc(MAX_TAB*sizeof(Revenu*));
    cmp1->depenses=(Depense**)malloc(MAX_TAB*sizeof(Depense*));
    return cmp1;
}
void delCompte(Compte * cmp1){
    free(cmp1);
}

int addRevenu(Compte * cmp,Revenu * rev){
    Revenu * rev2 = copyRev(rev);
    (cmp->revenus)[cmp->nbrev]=rev2;
    /*free(rev2->date);
    free(rev2);*/
    cmp->nbrev++;
    if(((reallocntRev)*MAX_TAB)-(cmp->nbrev)<=5){
        reallocntRev+=1;
        cmp->revenus=(Revenu**)realloc(cmp->revenus,reallocntRev*MAX_TAB*sizeof(Revenu*));
    }
    return 0;
}



int addDepense(Compte * cmp,Depense * dep){
    Depense * dep2 = copyDep(dep);
    (cmp->depenses)[cmp->nbdep]=dep2;
    /*free(dep2->date);
    free(dep2);*/
    cmp->nbdep++;
    if(((reallocntDep)*MAX_TAB)-(cmp->nbdep)<=5){
        reallocntDep+=1;
        cmp->depenses=(Depense**)realloc(cmp->depenses,reallocntDep*MAX_TAB*sizeof(Depense*));
    }
    return 0;
}

float total(Compte * cmp){
    int nbrev=cmp->nbrev;
    int nbdep=cmp->nbdep;
    int i;
    int j;
    float sum=0;
    for(i=0;i<nbrev;i++){
        sum+=(cmp->revenus)[i]->solde;
    }

    for(j=0;j<nbdep;j++){
        sum-=(cmp->depenses)[j]->solde;
    }

    return sum;
}

float totalCat(Compte * cmp, Type type){
    int nbrev=cmp->nbrev;
    int nbdep=cmp->nbdep;
    int i;
    int j;
    float sum=0;
    for(i=0;i<nbrev;i++){
        if((cmp->revenus)[i]->type==type){
            sum+=(cmp->revenus)[i]->solde;
        }
    }

    for(j=0;j<nbdep;j++){
        if((cmp->depenses)[j]->type==type){
            sum-=(cmp->depenses)[j]->solde;
        }
    }
    return sum;
}
