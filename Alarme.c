#include "Alarme.h"


Alarme * createAlarme(){

    Alarme *pt=(Alarme *)malloc(sizeof(Alarme));
    pt->alarmes=(int *)malloc(3*sizeof(int));
    pt->alarmes[0]=0;
    pt->alarmes[1]=0;
    pt->alarmes[2]=0;
    return pt;
}


void changeValAlarme(Alarme * a1, int a, int value){
    a1->alarmes[a]=value;

}

Alarme * copyAlarme(Alarme * a1){
    Alarme *pt=(Alarme *)malloc(sizeof(Alarme));
    pt->alarmes=(int *)malloc(3*sizeof(int));
    pt->alarmes[0]=a1->alarmes[0];
    pt->alarmes[1]=a1->alarmes[1];
    pt->alarmes[2]=a1->alarmes[2];
    return pt;

}
